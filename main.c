#include <kernel.h>
#include <stdlib.h>
#include <tamtypes.h>
#include <math3d.h>

#include <packet.h>

#include <dma_tags.h>
#include <gif_tags.h>
#include <gs_psm.h>

#include <dma.h>

#include <graph.h>

#include <draw.h>
#include <draw3d.h>

#define _TEXTURE

#include "mesh_data.c"

#ifdef _TEXTURE
extern unsigned char texture_start[];
#endif

VECTOR object_position = { 0.00f, 0.00f, 0.00f, 1.00f };
VECTOR object_rotation = { 0.00f, 0.00f, 0.00f, 1.00f };

VECTOR camera_position = { 0.00f, 0.00f, 60.00f, 1.00f };
VECTOR camera_rotation = { 0.00f, 0.00f,  0.00f, 1.00f };

#ifdef _TEXTURE
void init_gs(framebuffer_t *frame, zbuffer_t *z, texbuffer_t *texbuf)
#else
void init_gs(framebuffer_t *frame, zbuffer_t *z)
#endif
{

	// Define a 32-bit 640x512/448 framebuffer.
	frame->width = 640;
	frame->height = graph_get_region() == GRAPH_MODE_PAL ? 512 : 448;
	frame->mask = 0;
	frame->psm = GS_PSM_32;
	frame->address = graph_vram_allocate(frame->width, frame->height, frame->psm, GRAPH_ALIGN_PAGE);

	// Enable the zbuffer.
	z->enable = DRAW_ENABLE;
	z->mask = 0;
	z->method = ZTEST_METHOD_GREATER_EQUAL;
	z->zsm = GS_ZBUF_32;
	z->address = graph_vram_allocate(frame->width, frame->height, z->zsm, GRAPH_ALIGN_PAGE);

#ifdef _TEXTURE
	// Allocate some vram for the texture buffer.
	texbuf->width = 512;
	texbuf->psm = GS_PSM_24;
	texbuf->address = graph_vram_allocate(256, 256, GS_PSM_24, GRAPH_ALIGN_BLOCK);
#endif

	// Initialize the screen and tie the first framebuffer to the read circuits.
	graph_initialize(frame->address, frame->width, frame->height, frame->psm, 0, 0);

}

void init_drawing_environment(framebuffer_t *frame, zbuffer_t *z)
{

	packet_t *packet = packet_init(20, PACKET_NORMAL);

	// This is our generic qword pointer.
	qword_t *q = packet->data;

	// This will setup a default drawing environment.
	q = draw_setup_environment(q, 0, frame, z);

	// Now reset the primitive origin to 2048-width/2, 2048-height/2.
	q = draw_primitive_xyoffset(q, 0, (2048 - (frame->width / 2)), (2048 - (frame->height / 2)));

	// Finish setting up the environment.
	q = draw_finish(q);

	// Now send the packet, no need to wait since it's the first.
	dma_channel_send_normal(DMA_CHANNEL_GIF, packet->data, q - packet->data, 0, 0);
	dma_wait_fast();

	packet_free(packet);

}

#ifdef _TEXTURE
void load_texture(texbuffer_t *texbuf)
{

	packet_t *packet = packet_init(50, PACKET_NORMAL);

	qword_t *q = packet->data;

	q = packet->data;

	q = draw_texture_transfer(q, texture_start, 256, 256, GS_PSM_24, texbuf->address, texbuf->width);
	q = draw_texture_flush(q);

	dma_channel_send_chain(DMA_CHANNEL_GIF, packet->data, q - packet->data, 0,0);
	dma_wait_fast();

	packet_free(packet);

}

void setup_texture(texbuffer_t *texbuf)
{

	packet_t *packet = packet_init(10, PACKET_NORMAL);

	qword_t *q = packet->data;

	// Using a texture involves setting up a lot of information.
	clutbuffer_t clut;

	lod_t lod;

	lod.calculation = LOD_USE_K;
	lod.max_level = 0;
	lod.mag_filter = LOD_MAG_NEAREST;
	lod.min_filter = LOD_MIN_NEAREST;
	lod.l = 0;
	lod.k = 0;

	texbuf->info.width = draw_log2(256);
	texbuf->info.height = draw_log2(256);
	texbuf->info.components = TEXTURE_COMPONENTS_RGB;
	texbuf->info.function = TEXTURE_FUNCTION_DECAL;

	clut.storage_mode = CLUT_STORAGE_MODE1;
	clut.start = 0;
	clut.psm = 0;
	clut.load_method = CLUT_NO_LOAD;
	clut.address = 0;

	q = draw_texture_sampling(q, 0, &lod);
	q = draw_texturebuffer(q, 0, texbuf, &clut);

	// Now send the packet, no need to wait since it's the first.
	dma_channel_send_normal(DMA_CHANNEL_GIF, packet->data, q - packet->data, 0, 0);
	dma_wait_fast();

	packet_free(packet);

}
#endif

int render(framebuffer_t *frame, zbuffer_t *z)
{

	int i;
	int context = 0;

	// Matrices to setup the 3D environment and camera.
	MATRIX local_world;
	MATRIX world_view;
	MATRIX view_screen;
	MATRIX local_screen;

	VECTOR *temp_vertices;

	prim_t prim;
	color_t color;

	xyz_t   *xyz;
	color_t *rgbaq;
#ifdef _TEXTURE
	texel_t *st;
#endif

	// The data packets for double buffering dma sends.
	packet_t *packets[2];
	packet_t *current;
	qword_t *q;
#ifdef _TEXTURE
	u64 *dw;
#else
	qword_t *dmatag;
#endif

	packets[0] = packet_init(100, PACKET_NORMAL);
	packets[1] = packet_init(100, PACKET_NORMAL);

	// Allocate calculation space.
	temp_vertices = memalign(128, sizeof(VECTOR) * vertex_count);

	// Allocate register space.
#ifdef _TEXTURE
	xyz   = memalign(128, sizeof(u64) * vertex_count);
	rgbaq = memalign(128, sizeof(u64) * vertex_count);
	st    = memalign(128, sizeof(u64) * vertex_count);
#else
	xyz   = memalign(128, sizeof(vertex_t) * vertex_count);
	rgbaq = memalign(128, sizeof(color_t)  * vertex_count);
#endif

	// Define the triangle primitive we want to use.
	prim.type = PRIM_TRIANGLE;
	prim.shading = PRIM_SHADE_GOURAUD;
	prim.blending = DRAW_ENABLE;
#ifdef _TEXTURE
	prim.mapping = DRAW_ENABLE;
	prim.mapping_type = PRIM_MAP_ST;
	prim.blending = DRAW_ENABLE;
#else
	prim.mapping = DRAW_DISABLE;
	prim.mapping_type = DRAW_DISABLE;
	prim.blending = DRAW_DISABLE;
#endif
	prim.fogging = DRAW_DISABLE;
	prim.antialiasing = DRAW_DISABLE;
	prim.colorfix = PRIM_UNFIXED;

	color.r = 0x80;
	color.g = 0x80;
	color.b = 0x80;
#ifdef _TEXTURE
	color.a = 0x40;
#else
	color.a = 0x80;
#endif
	color.q = 1.0f;

	// Create the view_screen matrix.
	create_view_screen(view_screen, graph_aspect_ratio(), -3.00f, 3.00f, -3.00f, 3.00f, 1.00f, 2000.00f);

	// Wait for any previous dma transfers to finish before starting.
	dma_wait_fast();

	// The main loop...
	for (;;)
	{

		current = packets[context];

		// Spin the cube a bit.
		object_rotation[0] += 0.008f; while (object_rotation[0] > 3.14f) { object_rotation[0] -= 6.28f; }
		object_rotation[1] += 0.012f; while (object_rotation[1] > 3.14f) { object_rotation[1] -= 6.28f; }

		// Create the local_world matrix.
		create_local_world(local_world, object_position, object_rotation);

		// Create the world_view matrix.
		create_world_view(world_view, camera_position, camera_rotation);

		// Create the local_screen matrix.
		create_local_screen(local_screen, local_world, world_view, view_screen);

		// Calculate the vertex values.
		calculate_vertices(temp_vertices, vertex_count, vertices, local_screen);

		// Convert floating point vertices to fixed point and translate to center of screen.
		draw_convert_xyz(xyz, 2048, 2048, 32, vertex_count, (vertex_f_t*)temp_vertices);

		// Convert floating point colours to fixed point.
		draw_convert_rgbq(rgbaq, vertex_count, (vertex_f_t*)temp_vertices, (color_f_t*)colours, color.a);

#ifdef _TEXTURE
		// Generate the ST register values.
		draw_convert_st(st, vertex_count, (vertex_f_t*)temp_vertices, (texel_f_t*)coordinates);

		q = current->data;
#else
		// Grab our dmatag pointer for the dma chain.
		dmatag = current->data;

		// Now grab our qword pointer and increment past the dmatag.
		q = dmatag;
		q++;
#endif

		// Clear framebuffer but don't update zbuffer.
		q = draw_disable_tests(q, 0, z);
		q = draw_clear(q, 0, 2048.0f - (frame->width / 2), 2048.0f - (frame->height / 2) , frame->width, frame->height, 0x00, 0x00, 0x00);
		q = draw_enable_tests(q, 0, z);

		// Draw the triangles using triangle primitive type.
#ifdef _TEXTURE
		// Use a 64-bit pointer to simplify adding data to the packet.
		dw = (u64*)draw_prim_start(q, 0, &prim, &color);

		for (i = 0; i < points_count; i++)
		{
			*dw++ = rgbaq[points[i]].rgbaq;
			*dw++ = st[points[i]].uv;
			*dw++ = xyz[points[i]].xyz;
		}

		// Check if we're in middle of a qword or not.
		if ((u32)dw % 16)
		{
			*dw++ = 0;
		}

		// Only 3 registers rgbaq/st/xyz were used (standard STQ reglist).
		q = draw_prim_end((qword_t*)dw, 3, DRAW_STQ_REGLIST);
#else
		q = draw_prim_start(q, 0, &prim, &color);

		for (i = 0; i < points_count; i++)
		{
			q->dw[0] = rgbaq[points[i]].rgbaq;
			q->dw[1] = xyz[points[i]].xyz;
			q++;
		}

		q = draw_prim_end(q, 2, DRAW_RGBAQ_REGLIST);
#endif

		// Setup a finish event.
		q = draw_finish(q);

#ifndef _TEXTURE
		// Define our dmatag for the dma chain.
		DMATAG_END(dmatag, (q-current->data) - 1, 0, 0, 0);
#endif

		// Now send our current dma chain.
		dma_wait_fast();
#ifdef _TEXTURE
		dma_channel_send_normal(DMA_CHANNEL_GIF,current->data, q - current->data, 0, 0);
#else
		dma_channel_send_chain(DMA_CHANNEL_GIF, current->data, q - current->data, 0, 0);
#endif

		// Now switch our packets so we can process data while the DMAC is working.
		context ^= 1;

		// Wait for scene to finish drawing.
		draw_wait_finish();

		graph_wait_vsync();

	}

	packet_free(packets[0]);
	packet_free(packets[1]);

	return 0;

}

int main(int argc, char **argv)
{

	// The buffers to be used.
	framebuffer_t frame;
	zbuffer_t z;
#ifdef _TEXTURE
	texbuffer_t texbuf;
#endif

	// Init GIF dma channel.
	dma_channel_initialize(DMA_CHANNEL_GIF, NULL, 0);
	dma_channel_fast_waits(DMA_CHANNEL_GIF);

#ifdef _TEXTURE
	// Init the GS, framebuffer, zbuffer and texture buffer.
	init_gs(&frame, &z, &texbuf);
#else
	// Init the GS, framebuffer and zbuffer.
	init_gs(&frame, &z);
#endif

	// Init the drawing environment and framebuffer.
	init_drawing_environment(&frame, &z);

#ifdef _TEXTURE
	// Load the texture into vram.
	load_texture(&texbuf);

	// Setup texture buffer.
	setup_texture(&texbuf);
#endif

	// Render the cube.
	render(&frame, &z);

	// Sleep.
	SleepThread();

	// End program.
	return 0;

}
