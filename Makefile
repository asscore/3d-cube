EE_BIN = 3d-cube.elf
EE_BIN_PACKED = 3d-cube-packed.elf
EE_OBJS = main.o texture.o
EE_LIBS = -ldraw -lgraph -lmath3d -lmf -lpacket -ldma

all: $(EE_BIN)
	$(EE_STRIP) --strip-all $(EE_BIN)
	ps2-packer $(EE_BIN) $(EE_BIN_PACKED)

# png2raw karin.png
texture.o: texture.raw
	bin2o texture.raw texture.o texture

clean:
	rm -f $(EE_BIN) $(EE_BIN_PACKED) $(EE_OBJS)

run: $(EE_BIN)
	ps2client execee host:$(EE_BIN)

reset:
	ps2client reset


include $(PS2SDK)/samples/Makefile.pref
include $(PS2SDK)/samples/Makefile.eeglobal
